const mongoose = require('mongoose');
const config = require('../config');

const Connect = async() => {
    try {
        const con = await mongoose.connect(config.MONGO_URI, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: true
        });
        console.log('BD already!')
    } catch (err) {

        process.exit(1);
    }
};

module.exports = Connect;