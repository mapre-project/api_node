// API - MINISTERIO ADMINISTRATIVO DE LA PRESIDENCIA
// AUTOR: DANIEL VALDEZ
// EMAIL: HELLO@DANIEL-VALDEZ.COM
// TEL: +1(829) 696 - 7695
// WEBSITE: WWW.DANIEL-VALDEZ.COM

require('dotenv/config')
const serverless = require('serverless-http');
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const nodemailer = require('nodemailer');
const morgan = require('morgan');
const multer = require('multer')
const timeout = require('connect-timeout')
const uuid = require('uuid').v4;
const multerS3 = require('multer-s3');
const AWS = require('aws-sdk');
const app = express();
const { config } = require('dotenv');
config();

uuid();
app.use(morgan(':method :url :status :response-time ms'));
app.use('/', express.static(path.join(__dirname, 'static')));
app.use(express.json({ limit: '100mb', extended: true }));
app.use(express.urlencoded({ limit: '100mb', extended: true }))
app.use(cors());
const PORT = process.env.PORT || 5000;

const ID = "AKIAYHWLLO7HV42QGTFD"
const SECRET = "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"
const BUCKET_NAME = "files-mapre"

const s3 = new AWS.S3({
    accessKeyId: ID,
    secretAccessKey: SECRET
});

app.use('/', require('./routes/router'))


require('./database/database')();

app.listen(PORT, (req, res) => {
    console.log('Server up at port 5000')
})

const upload = multer({
    storage: multerS3({
        s3,
        bucket: BUCKET_NAME,
        metadata: (req, file, cb) => {
            cb(null, { fieldName: file.fieldname });
        },
        key: (req, file, cb) => {
            // const ext = path.extname(file.originalname);
            cb(null, `${req.params.id} - ${file.originalname}`);
        }
    })
});

const uploadFiles = multer({
    storage: multerS3({
        s3,
        bucket: BUCKET_NAME,
        metadata: (req, file, cb) => {
            cb(null, { fieldName: file.fieldname });
        },
        key: (req, file, cb) => {
            // const ext = path.extname(file.originalname);
            cb(null, `${req.params.id} - ${file.originalname}`);
        }
    })
});

app.use(express.static('public'))

app.post('/api/upload/:id', upload.array('files'), (req, res) => {
    return res.json({ status: 'OK', uploaded: req.files });
});
app.post('/api/upload/files/:id', uploadFiles.array('files'), (req, res) => {
    return res.json({ status: 'OK', uploaded: req.files });
});

app.use(cors());

// module.exports.handler = serverless(app);