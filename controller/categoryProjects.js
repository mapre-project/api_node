const { ServiceCatalogAppRegistry } = require('aws-sdk');
const category = require('../model/category_projects');
const projects = require('../model/projects')

exports.saveCategory = async(req, res) => {
    try {
        const newCategory = new category({
            projectName: req.body.projectName,
            typeAdjunto: req.body.adjunto,
            require: req.body.require
        })
        const categorySave = await newCategory.save()
        res.status(200).json("Datos guardados")

    } catch (err) {
        if (err.code === 11000) {
            res.status(500).json({ data: "Este registro ya existe", code: "101" })
        }

        if (err.message === 'CategoryProjectsSchema validation failed: typeAdjunto: Path `typeAdjunto` is required.') {
            res.status(500).json("El campo tipo de adjunto es requerido")
        }
        if (err.message === 'CategoryProjectsSchema validation failed: projectName: Path `projectName` is required.') {
            res.status(500).json("El campo tipo de proyecto es requerido")
        }
        if (err.message === 'CategoryProjectsSchema validation failed: require: Path `require` is required.') {
            res.status(500).json("El campo requerido es obligatorio")
        }
        // res.status(500).json(err)
    }
}

exports.saveProjects = async(req, res) => {
    try {
        const newProjects = new projects({
            projectName: req.body.projectName
        })
        const projectSave = await newProjects.save()
            .then(data => {
                res.status(200).json(data)

            })
    } catch (err) {
        if (err.code === 11000) {
            res.status(500).json({ data: "Este registro ya existe", code: "101" })
        }
    }
}

exports.validate = async(req, res) => {
    category.exists({ typeAdjunto: req.body.adjunto, projectName: req.body.projectName }, (error, result) => {
        if (error) {
            res.status(400).json(error)
        } else {
            if (result == true) {
                res.status(203).json('Este tipo de adjunto ya ha sido asociado al tipo de Proyecto')
            } else {
                res.status(200).json('El proyecto no existe')
            }

        }
    })
}

exports.saveDataC = async(req, res) => {
    try {
        const newCategory = new category({
            projectName: req.body.projectName,
            typeAdjunto: req.body.adjunto,
            require: req.body.require
        })
        const categorySave = await newCategory.save()
        res.status(200).json("Datos guardados")


    } catch (err) {
        res.status(500).json(err)
    }
}

exports.getQueryProject = async(req, res) => {
    category.find(req.query)
        .then(data => {
            res.status(200).json(data)
        })
        .catch((err) => {
            res.status(500).json(err)
        })
}

exports.getProjects = async(req, res) => {
    projects.find()
        .then(data => {
            res.status(200).json(data)
        })
        .catch((err) => {
            res.status(500).json(err)
        })
}

exports.viewCategory = async(req, res) => {
    category.find()
        .then(data => {
            res.status(200).json(data)
        })
        .catch((err) => {
            res.status(400).json(err)
            throw err;
        })
}

exports.findCategoryParams = async(req, res) => {
    category.findById({ _id: req.params._id })
        .then(data => {
            res.status(200).json(data)
        })
        .catch((err) => {
            res.status(500).json(err)
        })
}

exports.findProjectsParams = async(req, res) => {
    projects.findById({ _id: req.params._id })
        .then(data => {
            res.status(200).json(data)
        })
        .catch((err) => {
            res.status(500).json(err)
        })
}

exports.editCategory = async(req, res) => {
    category.findByIdAndUpdate({ _id: req.params._id }, req.body)
        .then(data => {
            res.status(200).json(data)
        })
        .catch((err) => {
            res.status(500).json(err)
        })
}

exports.editProjects = async(req, res) => {
    projects.findByIdAndUpdate({ _id: req.params._id }, req.body)
        .then(data => {
            res.status(200).json(data)
        })
        .catch((err) => {
            res.status(500).json(err)
        })
}

exports.deleteCategory = async(req, res) => {
    category.findByIdAndDelete({ _id: req.params._id })
        .then(data => {
            res.status(200).json(data)
        })
        .catch((err) => {
            res.status(500).json(err)
        })
}

exports.deleteProjects = async(req, res) => {
    projects.findByIdAndDelete({ _id: req.params._id })
        .then(data => {
            res.status(200).json(data)
        })
        .catch((err) => {
            res.status(500).json(err)
        })
}