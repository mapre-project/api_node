const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');
const moment = require('moment');

JWT_SECRET = "asdfasdfasdfasdgadfgnbqwiojbioasdnviuvhaorijqoifqiwhyrt9o2374ytr923ubhfo8921h3549gh298hfo82y3o45*/*/*/*/*/$ˆ%##ˆ%$@%&*ˆ#$%ˆ@#%ˆ#ˆ%$ˆ$ˆ}|{}{}}{}}????::::/***/*ˆ&*&"

const generate = (email) => {
    const date = new Date();
    date.setHours(date.getHours() + 24);
    return jwt.sign({ email, expiration: date }, JWT_SECRET)
}

exports.sendEmail_solicitud = async(req, res) => {

    const {
        email,
        name,
        categoria,
        tipo_de_informacion,
        comentarios,
        no_solicitud,
        id_solicitud
    } = req.body


    const token = generate(email);

    var transporter = nodemailer.createTransport({
    host: "smtp-mail.outlook.com", // hostname
    secureConnection: true, // TLS requires secureConnection to be false
    port: 587, // port for secure SMTP
    tls: {
       ciphers:'SSLv3'
    },
        auth: {
            user: 'laboratorio@juventud.gob.do',
            pass: '@UAhMT:.?QS3.?3'
        }
    })

    var mailOptions = {
        from: 'MINISTERIO DE LA JUVENTUD',
        to: email,
        subject: `Solicitud de Información - Proyecto ${no_solicitud}`,
        html: `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Solicitud de Información</h1>
    <br>
    <p>Estimado ${name}, por medio de este correo le solicitamos la siguiente información:</p>
    <br>
    <p>${comentarios}</p>
    <br>
    <p>Tipo de Información: ${tipo_de_informacion}</p>
    <br>
    <p>Categoria: ${categoria}</p>
    <br>
    <p>Haga click en este enlace para suministrar la información: <a href="https://solicitud.laboratorio.juventud.gob.do/file-solicitado/${id_solicitud}/${token}/${categoria}">https://solicitud.laboratorio.juventud.gob.do/file-solicitado/${id_solicitud}/${token}</a></p>
</body>
</html>`
    }

    transporter.sendMail(mailOptions, function(err, info) {
        if (err) {

            res.status(500).json('Error en el intento de enviar mail al destinatario');
        } else {

            res.status(200).json('Email enviado satisfactoriamente al destinatario')
        }
    })

}
exports.sendEmail_completar = async(req, res) => {

    const {
        email,
        name,
        id_solicitud,
        code,
        cedula
    } = req.body


    const token = generate(email);

    var transporter = nodemailer.createTransport({
    host: "smtp-mail.outlook.com", // hostname
    secureConnection: true, // TLS requires secureConnection to be false
    port: 587, // port for secure SMTP
    tls: {
       ciphers:'SSLv3'
    },
        auth: {
            user: 'laboratorio@juventud.gob.do',
            pass: '@UAhMT:.?QS3.?3'
        }
    })

    var mailOptions = {
        from: 'MINISTERIO DE LA JUVENTUD',
        to: email,
        subject: `Completar Proceso`,
        html: `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Solicitud de Información</h1>
    <br>
    <p>Estimado ${name}, por medio de este correo le informamos que ha iniciado el proceso de sometimiento de su proyecto pero no ha sido completado, para continuar siguiente el siguiente enlace lo redigira a continuar su proceso: <a href="https://consultar.laboratorio.juventud.gob.do/consultar">https://consultar.laboratorio.juventud.gob.do/consultar</a></p>
    <br>
    <p>Sus datos de acceso son:
    <br>
    Cedula: ${cedula}
    <br>
    No. de solicitud: ${code}
    </p>
    <br>
    <p>Si ya completo el proceso por favor ignorar este correo, muchas gracias.</p>
    <br>
</body>
</html>`
    }

    transporter.sendMail(mailOptions, function(err, info) {
        if (err) {

            res.status(500).json('Error en el intento de enviar mail al destinatario');
        } else {

            res.status(200).json('Email enviado satisfactoriamente al destinatario')
        }
    })

}


exports.sendEmail_reject = async(req, res) => {

    const {
        email,
    } = req.body

    var transporter = nodemailer.createTransport({
    host: "smtp-mail.outlook.com", // hostname
    secureConnection: true, // TLS requires secureConnection to be false
    port: 587, // port for secure SMTP
    tls: {
       ciphers:'SSLv3'
    },
        auth: {
            user: 'laboratorio@juventud.gob.do',
            pass: '@UAhMT:.?QS3.?3'
        }
    })

    var mailOptions = {
        from: 'MINISTERIO DE LA JUVENTUD',
        to: email,
        subject: `Proyecto rechazado`,
        html: `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Solicitud rechazada - MJ</h1>
    <br>
    <p>Lo sentimos, en estos momentos hemos tenido que rechazar tu solicitud, por favor intentalo mas adelante y esperamos porder apoyarte en tu proximo proyecto.</p>
</body>
</html>`
    }

    transporter.sendMail(mailOptions, function(err, info) {
        if (err) {

            res.status(500).json('Error en el intento de enviar mail al destinatario');
        } else {

            res.status(200).json('Email enviado satisfactoriamente al destinatario')
        }
    })

}


 function change(data) {
    let result = ''
    if(data === 'true') {
      result = 'Requerido'
    } else if(data === 'false') {
      result = 'No Requerido'
    }

    return result
  }

  function statusChange(e) {
               if (e == 'true') {
                 return 'Si'
               } else {
                 return 'No'
               }
             }

 function formatDate(e) {
    return moment(e).locale('es-do').format('LLL')
  }


 function printMunicipio(e) {
          var data = [
            {
              provincia_id : 1,
              minicipio_id : 101,
              minicipio : "SANTO DOMINGO CENTRO (DN)"
            },
            {
              provincia_id : 71,
              minicipio_id : 201,
              minicipio : "AZUA"
            },
            {
              provincia_id : 71,
              minicipio_id : 202,
              minicipio : "LAS CHARCAS"
            },
            {
              provincia_id : 71,
              minicipio_id : 203,
              minicipio : "LAS YAYAS DE VIAJAMA"
            },
            {
              provincia_id : 71,
              minicipio_id : 204,
              minicipio : "PADRE LAS CASAS"
            },
            {
              provincia_id : 71,
              minicipio_id : 205,
              minicipio : "PERALTA"
            },
            {
              provincia_id : 71,
              minicipio_id : 206,
              minicipio : "SABANA YEGUA"
            },
            {
              provincia_id : 71,
              minicipio_id : 207,
              minicipio : "PUEBLO VIEJO"
            },
            {
              provincia_id : 71,
              minicipio_id : 208,
              minicipio : "TABARA ARRIBA"
            },
            {
              provincia_id : 71,
              minicipio_id : 209,
              minicipio : "GUAYABAL"
            },
            {
              provincia_id : 71,
              minicipio_id : 210,
              minicipio : "ESTEBANIA"
            },
            {
              provincia_id : 82,
              minicipio_id : 301,
              minicipio : "NEIBA"
            },
            {
              provincia_id : 82,
              minicipio_id : 302,
              minicipio : "GALVAN"
            },
            {
              provincia_id : 82,
              minicipio_id : 303,
              minicipio : "TAMAYO"
            },
            {
              provincia_id : 82,
              minicipio_id : 304,
              minicipio : "VILLA JARAGUA"
            },
            {
              provincia_id : 82,
              minicipio_id : 305,
              minicipio : "LOS RIOS"
            },
            {
              provincia_id : 81,
              minicipio_id : 401,
              minicipio : "BARAHONA"
            },
            {
              provincia_id : 81,
              minicipio_id : 402,
              minicipio : "CABRAL"
            },
            {
              provincia_id : 81,
              minicipio_id : 403,
              minicipio : "ENRIQUILLO"
            },
            {
              provincia_id : 81,
              minicipio_id : 404,
              minicipio : "PARAISO"
            },
            {
              provincia_id : 81,
              minicipio_id : 405,
              minicipio : "VICENTE NOBLE"
            },
            {
              provincia_id : 81,
              minicipio_id : 406,
              minicipio : "EL PEÑON"
            },
            {
              provincia_id : 81,
              minicipio_id : 407,
              minicipio : "LA CIENAGA"
            },
            {
              provincia_id : 81,
              minicipio_id : 408,
              minicipio : "FUNDACION"
            },
            {
              provincia_id : 81,
              minicipio_id : 409,
              minicipio : "LAS SALINAS"
            },
            {
              provincia_id : 81,
              minicipio_id : 410,
              minicipio : "POLO"
            },
            {
              provincia_id : 81,
              minicipio_id : 411,
              minicipio : "JAQUIMEYES"
            },
            {
              provincia_id : 63,
              minicipio_id : 501,
              minicipio : "DAJABON"
            },
            {
              provincia_id : 63,
              minicipio_id : 502,
              minicipio : "LOMA DE CABRERA"
            },
            {
              provincia_id : 63,
              minicipio_id : 503,
              minicipio : "PARTIDO"
            },
            {
              provincia_id : 63,
              minicipio_id : 504,
              minicipio : "RESTAURACION"
            },
            {
              provincia_id : 63,
              minicipio_id : 505,
              minicipio : "EL PINO"
            },
            {
              provincia_id : 31,
              minicipio_id : 601,
              minicipio : "SAN FRANCISCO DE MACORIS"
            },
            {
              provincia_id : 31,
              minicipio_id : 602,
              minicipio : "ARENOSO"
            },
            {
              provincia_id : 31,
              minicipio_id : 603,
              minicipio : "CASTILLO"
            },
            {
              provincia_id : 31,
              minicipio_id : 604,
              minicipio : "PIMENTEL"
            },
            {
              provincia_id : 31,
              minicipio_id : 605,
              minicipio : "VILLA RIVA"
            },
            {
              provincia_id : 31,
              minicipio_id : 606,
              minicipio : "LAS GUARANAS"
            },
            {
              provincia_id : 31,
              minicipio_id : 607,
              minicipio : "EUGENIO MARIA DE HOSTOS"
            },
            {
              provincia_id : 73,
              minicipio_id : 701,
              minicipio : "COMENDADOR"
            },
            {
              provincia_id : 73,
              minicipio_id : 702,
              minicipio : "BANICA"
            },
            {
              provincia_id : 73,
              minicipio_id : 703,
              minicipio : "EL LLANO"
            },
            {
              provincia_id : 73,
              minicipio_id : 704,
              minicipio : "HONDO VALLE"
            },
            {
              provincia_id : 73,
              minicipio_id : 705,
              minicipio : "PEDRO SANTANA"
            },
            {
              provincia_id : 73,
              minicipio_id : 706,
              minicipio : "JUAN SANTIAGO"
            },
            {
              provincia_id : 24,
              minicipio_id : 801,
              minicipio : "EL SEIBO"
            },
            {
              provincia_id : 24,
              minicipio_id : 802,
              minicipio : "MICHES"
            },
            {
              provincia_id : 56,
              minicipio_id : 901,
              minicipio : "MOCA"
            },
            {
              provincia_id : 56,
              minicipio_id : 902,
              minicipio : "CAYETANO GERMOSEN"
            },
            {
              provincia_id : 56,
              minicipio_id : 903,
              minicipio : "GASPAR HERNANDEZ"
            },
            {
              provincia_id : 56,
              minicipio_id : 904,
              minicipio : "JAMAO AL NORTE"
            },
            {
              provincia_id : 83,
              minicipio_id : 1001,
              minicipio : "JIMANI"
            },
            {
              provincia_id : 83,
              minicipio_id : 1002,
              minicipio : "DUVERGE"
            },
            {
              provincia_id : 83,
              minicipio_id : 1003,
              minicipio : "LA DESCUBIERTA"
            },
            {
              provincia_id : 83,
              minicipio_id : 1004,
              minicipio : "POSTRER RIO"
            },
            {
              provincia_id : 83,
              minicipio_id : 1005,
              minicipio : "CRISTOBAL"
            },
            {
              provincia_id : 83,
              minicipio_id : 1006,
              minicipio : "MELLA"
            },
            {
              provincia_id : 23,
              minicipio_id : 1101,
              minicipio : "HIGUEY"
            },
            {
              provincia_id : 23,
              minicipio_id : 1102,
              minicipio : "SAN RAFAEL DEL YUMA"
            },
            {
              provincia_id : 22,
              minicipio_id : 1201,
              minicipio : "LA ROMANA"
            },
            {
              provincia_id : 22,
              minicipio_id : 1202,
              minicipio : "GUAYMATE"
            },
            {
              provincia_id : 22,
              minicipio_id : 1203,
              minicipio : "VILLA HERMOSA"
            },
            {
              provincia_id : 41,
              minicipio_id : 1301,
              minicipio : "LA VEGA"
            },
            {
              provincia_id : 41,
              minicipio_id : 1302,
              minicipio : "CONSTANZA"
            },
            {
              provincia_id : 41,
              minicipio_id : 1303,
              minicipio : "JARABACOA"
            },
            {
              provincia_id : 41,
              minicipio_id : 1304,
              minicipio : "JIMA ABAJO"
            },
            {
              provincia_id : 33,
              minicipio_id : 1401,
              minicipio : "NAGUA"
            },
            {
              provincia_id : 33,
              minicipio_id : 1402,
              minicipio : "CABRERA"
            },
            {
              provincia_id : 33,
              minicipio_id : 1403,
              minicipio : "EL FACTOR"
            },
            {
              provincia_id : 33,
              minicipio_id : 1404,
              minicipio : "RIO SAN JUAN"
            },
            {
              provincia_id : 62,
              minicipio_id : 1501,
              minicipio : "MONTE CRISTI"
            },
            {
              provincia_id : 62,
              minicipio_id : 1502,
              minicipio : "CASTAÃ‘UELAS"
            },
            {
              provincia_id : 62,
              minicipio_id : 1503,
              minicipio : "GUAYUBIN"
            },
            {
              provincia_id : 62,
              minicipio_id : 1504,
              minicipio : "LAS MATAS DE SANTA CRUZ"
            },
            {
              provincia_id : 62,
              minicipio_id : 1505,
              minicipio : "PEPILLO SALCEDO"
            },
            {
              provincia_id : 62,
              minicipio_id : 1506,
              minicipio : "VILLA VASQUEZ"
            },
            {
              provincia_id : 84,
              minicipio_id : 1601,
              minicipio : "PEDERNALES"
            },
            {
              provincia_id : 84,
              minicipio_id : 1602,
              minicipio : "OVIEDO"
            },
            {
              provincia_id : 94,
              minicipio_id : 1701,
              minicipio : "BANI"
            },
            {
              provincia_id : 94,
              minicipio_id : 1702,
              minicipio : "NIZAO"
            },
            {
              provincia_id : 57,
              minicipio_id : 1801,
              minicipio : "PUERTO PLATA"
            },
            {
              provincia_id : 57,
              minicipio_id : 1802,
              minicipio : "ALTAMIRA"
            },
            {
              provincia_id : 57,
              minicipio_id : 1803,
              minicipio : "GUANANICO"
            },
            {
              provincia_id : 57,
              minicipio_id : 1804,
              minicipio : "IMBERT"
            },
            {
              provincia_id : 57,
              minicipio_id : 1805,
              minicipio : "LOS HIDALGOS"
            },
            {
              provincia_id : 57,
              minicipio_id : 1806,
              minicipio : "LUPERON"
            },
            {
              provincia_id : 57,
              minicipio_id : 1807,
              minicipio : "SOSUA"
            },
            {
              provincia_id : 57,
              minicipio_id : 1808,
              minicipio : "VILLA ISABELA"
            },
            {
              provincia_id : 57,
              minicipio_id : 1809,
              minicipio : "VILLA MONTELLANO"
            },
            {
              provincia_id : 34,
              minicipio_id : 1901,
              minicipio : "SALCEDO"
            },
            {
              provincia_id : 34,
              minicipio_id : 1902,
              minicipio : "TENARES"
            },
            {
              provincia_id : 34,
              minicipio_id : 1903,
              minicipio : "VILLA TAPIA"
            },
            {
              provincia_id : 32,
              minicipio_id : 2001,
              minicipio : "SAMANA"
            },
            {
              provincia_id : 32,
              minicipio_id : 2002,
              minicipio : "SANCHEZ"
            },
            {
              provincia_id : 32,
              minicipio_id : 2003,
              minicipio : "LAS TERRENAS"
            },
            {
              provincia_id : 91,
              minicipio_id : 2101,
              minicipio : "SAN CRISTOBAL"
            },
            {
              provincia_id : 91,
              minicipio_id : 2102,
              minicipio : "SABANA GRANDE DE PALENQUE"
            },
            {
              provincia_id : 91,
              minicipio_id : 2103,
              minicipio : "BAJOS DE HAINA"
            },
            {
              provincia_id : 91,
              minicipio_id : 2104,
              minicipio : "CAMBITA GARABITOS"
            },
            {
              provincia_id : 91,
              minicipio_id : 2105,
              minicipio : "VILLA ALTAGRACIA"
            },
            {
              provincia_id : 91,
              minicipio_id : 2106,
              minicipio : "YAGUATE"
            },
            {
              provincia_id : 91,
              minicipio_id : 2107,
              minicipio : "SAN GREGORIO DE NIGUA"
            },
            {
              provincia_id : 91,
              minicipio_id : 2108,
              minicipio : "LOS CACAOS"
            },
            {
              provincia_id : 72,
              minicipio_id : 2201,
              minicipio : "SAN JUAN"
            },
            {
              provincia_id : 72,
              minicipio_id : 2202,
              minicipio : "BOHECHIO"
            },
            {
              provincia_id : 72,
              minicipio_id : 2203,
              minicipio : "EL CERCADO"
            },
            {
              provincia_id : 72,
              minicipio_id : 2204,
              minicipio : "JUAN DE HERRERA"
            },
            {
              provincia_id : 72,
              minicipio_id : 2205,
              minicipio : "LAS MATAS DE FARFAN"
            },
            {
              provincia_id : 72,
              minicipio_id : 2206,
              minicipio : "VALLEJUELO"
            },
            {
              provincia_id : 21,
              minicipio_id : 2301,
              minicipio : "SAN PEDRO DE MACORIS"
            },
            {
              provincia_id : 21,
              minicipio_id : 2302,
              minicipio : "LOS LLANOS"
            },
            {
              provincia_id : 21,
              minicipio_id : 2303,
              minicipio : "RAMON SANTANA"
            },
            {
              provincia_id : 21,
              minicipio_id : 2304,
              minicipio : "CONSUELO"
            },
            {
              provincia_id : 21,
              minicipio_id : 2305,
              minicipio : "QUISQUEYA"
            },
            {
              provincia_id : 21,
              minicipio_id : 2306,
              minicipio : "GUAYACANES"
            },
            {
              provincia_id : 43,
              minicipio_id : 2401,
              minicipio : "COTUI"
            },
            {
              provincia_id : 43,
              minicipio_id : 2402,
              minicipio : "CEVICOS"
            },
            {
              provincia_id : 43,
              minicipio_id : 2403,
              minicipio : "FANTINO"
            },
            {
              provincia_id : 43,
              minicipio_id : 2404,
              minicipio : "LA MATA"
            },
            {
              provincia_id : 51,
              minicipio_id : 2501,
              minicipio : "SANTIAGO"
            },
            {
              provincia_id : 51,
              minicipio_id : 2502,
              minicipio : "BISONO"
            },
            {
              provincia_id : 51,
              minicipio_id : 2503,
              minicipio : "JANICO"
            },
            {
              provincia_id : 51,
              minicipio_id : 2504,
              minicipio : "LICEY AL MEDIO"
            },
            {
              provincia_id : 51,
              minicipio_id : 2505,
              minicipio : "SAN JOSE DE LAS MATAS"
            },
            {
              provincia_id : 51,
              minicipio_id : 2506,
              minicipio : "TAMBORIL"
            },
            {
              provincia_id : 51,
              minicipio_id : 2507,
              minicipio : "VILLA GONZALEZ"
            },
            {
              provincia_id : 51,
              minicipio_id : 2508,
              minicipio : "PUÑAL"
            },
            {
              provincia_id : 51,
              minicipio_id : 2509,
              minicipio : "SABANA IGLESIA"
            },
            {
              provincia_id : 64,
              minicipio_id : 2601,
              minicipio : "SAN IGNACIO DE SABANETA"
            },
            {
              provincia_id : 64,
              minicipio_id : 2602,
              minicipio : "VILLA LOS ALMACIGOS"
            },
            {
              provincia_id : 64,
              minicipio_id : 2603,
              minicipio : "MONCION"
            },
            {
              provincia_id : 61,
              minicipio_id : 2701,
              minicipio : "MAO"
            },
            {
              provincia_id : 61,
              minicipio_id : 2702,
              minicipio : "ESPERANZA"
            },
            {
              provincia_id : 61,
              minicipio_id : 2703,
              minicipio : "LAGUNA SALADA"
            },
            {
              provincia_id : 42,
              minicipio_id : 2801,
              minicipio : "BONAO"
            },
            {
              provincia_id : 42,
              minicipio_id : 2802,
              minicipio : "MAIMON"
            },
            {
              provincia_id : 42,
              minicipio_id : 2803,
              minicipio : "PIEDRA BLANCA"
            },
            {
              provincia_id : 92,
              minicipio_id : 2901,
              minicipio : "MONTE PLATA"
            },
            {
              provincia_id : 92,
              minicipio_id : 2902,
              minicipio : "BAYAGUANA"
            },
            {
              provincia_id : 92,
              minicipio_id : 2903,
              minicipio : "SABANA GRANDE DE BOYA"
            },
            {
              provincia_id : 92,
              minicipio_id : 2904,
              minicipio : "YAMASA"
            },
            {
              provincia_id : 92,
              minicipio_id : 2905,
              minicipio : "PERALVILLO"
            },
            {
              provincia_id : 25,
              minicipio_id : 3001,
              minicipio : "HATO MAYOR"
            },
            {
              provincia_id : 25,
              minicipio_id : 3002,
              minicipio : "SABANA DE LA MAR"
            },
            {
              provincia_id : 25,
              minicipio_id : 3003,
              minicipio : "EL VALLE"
            },
            {
              provincia_id : 93,
              minicipio_id : 3101,
              minicipio : "SAN JOSE DE OCOA"
            },
            {
              provincia_id : 93,
              minicipio_id : 3102,
              minicipio : "SABANA LARGA"
            },
            {
              provincia_id : 93,
              minicipio_id : 3103,
              minicipio : "RANCHO ARRIBA"
            },
            {
              provincia_id : 1,
              minicipio_id : 3201,
              minicipio : "SANTO DOMINGO ESTE"
            },
            {
              provincia_id : 1,
              minicipio_id : 3202,
              minicipio : "SANTO DOMINGO OESTE"
            },
            {
              provincia_id : 1,
              minicipio_id : 3203,
              minicipio : "SANTO DOMINGO NORTE"
            },
            {
              provincia_id : 1,
              minicipio_id : 3204,
              minicipio : "BOCA CHICA"
            },
            {
              provincia_id : 1,
              minicipio_id : 3205,
              minicipio : "SAN ANTONIO DE GUERRA"
            },
            {
              provincia_id : 1,
              minicipio_id : 3206,
              minicipio : "LOS ALCARRIZOS"
            },
            {
              provincia_id : 1,
              minicipio_id : 3207,
              minicipio : "PEDRO BRAND"
            }
            ]

          let mu = e

          let result = data.filter(x => x.minicipio_id == e)
          const str = result[0]?.minicipio.toLocaleLowerCase();
          const str2 = str?.charAt(0).toUpperCase() + str?.slice(1);
          return str2

    }
 function printMunicipio2(e, reside) {
 console.log(reside);
        if(reside == 'false') {
          return "N/A";
        } else {
          var data = [
            {
              provincia_id : 1,
              minicipio_id : 101,
              minicipio : "SANTO DOMINGO CENTRO (DN)"
            },
            {
              provincia_id : 71,
              minicipio_id : 201,
              minicipio : "AZUA"
            },
            {
              provincia_id : 71,
              minicipio_id : 202,
              minicipio : "LAS CHARCAS"
            },
            {
              provincia_id : 71,
              minicipio_id : 203,
              minicipio : "LAS YAYAS DE VIAJAMA"
            },
            {
              provincia_id : 71,
              minicipio_id : 204,
              minicipio : "PADRE LAS CASAS"
            },
            {
              provincia_id : 71,
              minicipio_id : 205,
              minicipio : "PERALTA"
            },
            {
              provincia_id : 71,
              minicipio_id : 206,
              minicipio : "SABANA YEGUA"
            },
            {
              provincia_id : 71,
              minicipio_id : 207,
              minicipio : "PUEBLO VIEJO"
            },
            {
              provincia_id : 71,
              minicipio_id : 208,
              minicipio : "TABARA ARRIBA"
            },
            {
              provincia_id : 71,
              minicipio_id : 209,
              minicipio : "GUAYABAL"
            },
            {
              provincia_id : 71,
              minicipio_id : 210,
              minicipio : "ESTEBANIA"
            },
            {
              provincia_id : 82,
              minicipio_id : 301,
              minicipio : "NEIBA"
            },
            {
              provincia_id : 82,
              minicipio_id : 302,
              minicipio : "GALVAN"
            },
            {
              provincia_id : 82,
              minicipio_id : 303,
              minicipio : "TAMAYO"
            },
            {
              provincia_id : 82,
              minicipio_id : 304,
              minicipio : "VILLA JARAGUA"
            },
            {
              provincia_id : 82,
              minicipio_id : 305,
              minicipio : "LOS RIOS"
            },
            {
              provincia_id : 81,
              minicipio_id : 401,
              minicipio : "BARAHONA"
            },
            {
              provincia_id : 81,
              minicipio_id : 402,
              minicipio : "CABRAL"
            },
            {
              provincia_id : 81,
              minicipio_id : 403,
              minicipio : "ENRIQUILLO"
            },
            {
              provincia_id : 81,
              minicipio_id : 404,
              minicipio : "PARAISO"
            },
            {
              provincia_id : 81,
              minicipio_id : 405,
              minicipio : "VICENTE NOBLE"
            },
            {
              provincia_id : 81,
              minicipio_id : 406,
              minicipio : "EL PEÑON"
            },
            {
              provincia_id : 81,
              minicipio_id : 407,
              minicipio : "LA CIENAGA"
            },
            {
              provincia_id : 81,
              minicipio_id : 408,
              minicipio : "FUNDACION"
            },
            {
              provincia_id : 81,
              minicipio_id : 409,
              minicipio : "LAS SALINAS"
            },
            {
              provincia_id : 81,
              minicipio_id : 410,
              minicipio : "POLO"
            },
            {
              provincia_id : 81,
              minicipio_id : 411,
              minicipio : "JAQUIMEYES"
            },
            {
              provincia_id : 63,
              minicipio_id : 501,
              minicipio : "DAJABON"
            },
            {
              provincia_id : 63,
              minicipio_id : 502,
              minicipio : "LOMA DE CABRERA"
            },
            {
              provincia_id : 63,
              minicipio_id : 503,
              minicipio : "PARTIDO"
            },
            {
              provincia_id : 63,
              minicipio_id : 504,
              minicipio : "RESTAURACION"
            },
            {
              provincia_id : 63,
              minicipio_id : 505,
              minicipio : "EL PINO"
            },
            {
              provincia_id : 31,
              minicipio_id : 601,
              minicipio : "SAN FRANCISCO DE MACORIS"
            },
            {
              provincia_id : 31,
              minicipio_id : 602,
              minicipio : "ARENOSO"
            },
            {
              provincia_id : 31,
              minicipio_id : 603,
              minicipio : "CASTILLO"
            },
            {
              provincia_id : 31,
              minicipio_id : 604,
              minicipio : "PIMENTEL"
            },
            {
              provincia_id : 31,
              minicipio_id : 605,
              minicipio : "VILLA RIVA"
            },
            {
              provincia_id : 31,
              minicipio_id : 606,
              minicipio : "LAS GUARANAS"
            },
            {
              provincia_id : 31,
              minicipio_id : 607,
              minicipio : "EUGENIO MARIA DE HOSTOS"
            },
            {
              provincia_id : 73,
              minicipio_id : 701,
              minicipio : "COMENDADOR"
            },
            {
              provincia_id : 73,
              minicipio_id : 702,
              minicipio : "BANICA"
            },
            {
              provincia_id : 73,
              minicipio_id : 703,
              minicipio : "EL LLANO"
            },
            {
              provincia_id : 73,
              minicipio_id : 704,
              minicipio : "HONDO VALLE"
            },
            {
              provincia_id : 73,
              minicipio_id : 705,
              minicipio : "PEDRO SANTANA"
            },
            {
              provincia_id : 73,
              minicipio_id : 706,
              minicipio : "JUAN SANTIAGO"
            },
            {
              provincia_id : 24,
              minicipio_id : 801,
              minicipio : "EL SEIBO"
            },
            {
              provincia_id : 24,
              minicipio_id : 802,
              minicipio : "MICHES"
            },
            {
              provincia_id : 56,
              minicipio_id : 901,
              minicipio : "MOCA"
            },
            {
              provincia_id : 56,
              minicipio_id : 902,
              minicipio : "CAYETANO GERMOSEN"
            },
            {
              provincia_id : 56,
              minicipio_id : 903,
              minicipio : "GASPAR HERNANDEZ"
            },
            {
              provincia_id : 56,
              minicipio_id : 904,
              minicipio : "JAMAO AL NORTE"
            },
            {
              provincia_id : 83,
              minicipio_id : 1001,
              minicipio : "JIMANI"
            },
            {
              provincia_id : 83,
              minicipio_id : 1002,
              minicipio : "DUVERGE"
            },
            {
              provincia_id : 83,
              minicipio_id : 1003,
              minicipio : "LA DESCUBIERTA"
            },
            {
              provincia_id : 83,
              minicipio_id : 1004,
              minicipio : "POSTRER RIO"
            },
            {
              provincia_id : 83,
              minicipio_id : 1005,
              minicipio : "CRISTOBAL"
            },
            {
              provincia_id : 83,
              minicipio_id : 1006,
              minicipio : "MELLA"
            },
            {
              provincia_id : 23,
              minicipio_id : 1101,
              minicipio : "HIGUEY"
            },
            {
              provincia_id : 23,
              minicipio_id : 1102,
              minicipio : "SAN RAFAEL DEL YUMA"
            },
            {
              provincia_id : 22,
              minicipio_id : 1201,
              minicipio : "LA ROMANA"
            },
            {
              provincia_id : 22,
              minicipio_id : 1202,
              minicipio : "GUAYMATE"
            },
            {
              provincia_id : 22,
              minicipio_id : 1203,
              minicipio : "VILLA HERMOSA"
            },
            {
              provincia_id : 41,
              minicipio_id : 1301,
              minicipio : "LA VEGA"
            },
            {
              provincia_id : 41,
              minicipio_id : 1302,
              minicipio : "CONSTANZA"
            },
            {
              provincia_id : 41,
              minicipio_id : 1303,
              minicipio : "JARABACOA"
            },
            {
              provincia_id : 41,
              minicipio_id : 1304,
              minicipio : "JIMA ABAJO"
            },
            {
              provincia_id : 33,
              minicipio_id : 1401,
              minicipio : "NAGUA"
            },
            {
              provincia_id : 33,
              minicipio_id : 1402,
              minicipio : "CABRERA"
            },
            {
              provincia_id : 33,
              minicipio_id : 1403,
              minicipio : "EL FACTOR"
            },
            {
              provincia_id : 33,
              minicipio_id : 1404,
              minicipio : "RIO SAN JUAN"
            },
            {
              provincia_id : 62,
              minicipio_id : 1501,
              minicipio : "MONTE CRISTI"
            },
            {
              provincia_id : 62,
              minicipio_id : 1502,
              minicipio : "CASTAÃ‘UELAS"
            },
            {
              provincia_id : 62,
              minicipio_id : 1503,
              minicipio : "GUAYUBIN"
            },
            {
              provincia_id : 62,
              minicipio_id : 1504,
              minicipio : "LAS MATAS DE SANTA CRUZ"
            },
            {
              provincia_id : 62,
              minicipio_id : 1505,
              minicipio : "PEPILLO SALCEDO"
            },
            {
              provincia_id : 62,
              minicipio_id : 1506,
              minicipio : "VILLA VASQUEZ"
            },
            {
              provincia_id : 84,
              minicipio_id : 1601,
              minicipio : "PEDERNALES"
            },
            {
              provincia_id : 84,
              minicipio_id : 1602,
              minicipio : "OVIEDO"
            },
            {
              provincia_id : 94,
              minicipio_id : 1701,
              minicipio : "BANI"
            },
            {
              provincia_id : 94,
              minicipio_id : 1702,
              minicipio : "NIZAO"
            },
            {
              provincia_id : 57,
              minicipio_id : 1801,
              minicipio : "PUERTO PLATA"
            },
            {
              provincia_id : 57,
              minicipio_id : 1802,
              minicipio : "ALTAMIRA"
            },
            {
              provincia_id : 57,
              minicipio_id : 1803,
              minicipio : "GUANANICO"
            },
            {
              provincia_id : 57,
              minicipio_id : 1804,
              minicipio : "IMBERT"
            },
            {
              provincia_id : 57,
              minicipio_id : 1805,
              minicipio : "LOS HIDALGOS"
            },
            {
              provincia_id : 57,
              minicipio_id : 1806,
              minicipio : "LUPERON"
            },
            {
              provincia_id : 57,
              minicipio_id : 1807,
              minicipio : "SOSUA"
            },
            {
              provincia_id : 57,
              minicipio_id : 1808,
              minicipio : "VILLA ISABELA"
            },
            {
              provincia_id : 57,
              minicipio_id : 1809,
              minicipio : "VILLA MONTELLANO"
            },
            {
              provincia_id : 34,
              minicipio_id : 1901,
              minicipio : "SALCEDO"
            },
            {
              provincia_id : 34,
              minicipio_id : 1902,
              minicipio : "TENARES"
            },
            {
              provincia_id : 34,
              minicipio_id : 1903,
              minicipio : "VILLA TAPIA"
            },
            {
              provincia_id : 32,
              minicipio_id : 2001,
              minicipio : "SAMANA"
            },
            {
              provincia_id : 32,
              minicipio_id : 2002,
              minicipio : "SANCHEZ"
            },
            {
              provincia_id : 32,
              minicipio_id : 2003,
              minicipio : "LAS TERRENAS"
            },
            {
              provincia_id : 91,
              minicipio_id : 2101,
              minicipio : "SAN CRISTOBAL"
            },
            {
              provincia_id : 91,
              minicipio_id : 2102,
              minicipio : "SABANA GRANDE DE PALENQUE"
            },
            {
              provincia_id : 91,
              minicipio_id : 2103,
              minicipio : "BAJOS DE HAINA"
            },
            {
              provincia_id : 91,
              minicipio_id : 2104,
              minicipio : "CAMBITA GARABITOS"
            },
            {
              provincia_id : 91,
              minicipio_id : 2105,
              minicipio : "VILLA ALTAGRACIA"
            },
            {
              provincia_id : 91,
              minicipio_id : 2106,
              minicipio : "YAGUATE"
            },
            {
              provincia_id : 91,
              minicipio_id : 2107,
              minicipio : "SAN GREGORIO DE NIGUA"
            },
            {
              provincia_id : 91,
              minicipio_id : 2108,
              minicipio : "LOS CACAOS"
            },
            {
              provincia_id : 72,
              minicipio_id : 2201,
              minicipio : "SAN JUAN"
            },
            {
              provincia_id : 72,
              minicipio_id : 2202,
              minicipio : "BOHECHIO"
            },
            {
              provincia_id : 72,
              minicipio_id : 2203,
              minicipio : "EL CERCADO"
            },
            {
              provincia_id : 72,
              minicipio_id : 2204,
              minicipio : "JUAN DE HERRERA"
            },
            {
              provincia_id : 72,
              minicipio_id : 2205,
              minicipio : "LAS MATAS DE FARFAN"
            },
            {
              provincia_id : 72,
              minicipio_id : 2206,
              minicipio : "VALLEJUELO"
            },
            {
              provincia_id : 21,
              minicipio_id : 2301,
              minicipio : "SAN PEDRO DE MACORIS"
            },
            {
              provincia_id : 21,
              minicipio_id : 2302,
              minicipio : "LOS LLANOS"
            },
            {
              provincia_id : 21,
              minicipio_id : 2303,
              minicipio : "RAMON SANTANA"
            },
            {
              provincia_id : 21,
              minicipio_id : 2304,
              minicipio : "CONSUELO"
            },
            {
              provincia_id : 21,
              minicipio_id : 2305,
              minicipio : "QUISQUEYA"
            },
            {
              provincia_id : 21,
              minicipio_id : 2306,
              minicipio : "GUAYACANES"
            },
            {
              provincia_id : 43,
              minicipio_id : 2401,
              minicipio : "COTUI"
            },
            {
              provincia_id : 43,
              minicipio_id : 2402,
              minicipio : "CEVICOS"
            },
            {
              provincia_id : 43,
              minicipio_id : 2403,
              minicipio : "FANTINO"
            },
            {
              provincia_id : 43,
              minicipio_id : 2404,
              minicipio : "LA MATA"
            },
            {
              provincia_id : 51,
              minicipio_id : 2501,
              minicipio : "SANTIAGO"
            },
            {
              provincia_id : 51,
              minicipio_id : 2502,
              minicipio : "BISONO"
            },
            {
              provincia_id : 51,
              minicipio_id : 2503,
              minicipio : "JANICO"
            },
            {
              provincia_id : 51,
              minicipio_id : 2504,
              minicipio : "LICEY AL MEDIO"
            },
            {
              provincia_id : 51,
              minicipio_id : 2505,
              minicipio : "SAN JOSE DE LAS MATAS"
            },
            {
              provincia_id : 51,
              minicipio_id : 2506,
              minicipio : "TAMBORIL"
            },
            {
              provincia_id : 51,
              minicipio_id : 2507,
              minicipio : "VILLA GONZALEZ"
            },
            {
              provincia_id : 51,
              minicipio_id : 2508,
              minicipio : "PUÑAL"
            },
            {
              provincia_id : 51,
              minicipio_id : 2509,
              minicipio : "SABANA IGLESIA"
            },
            {
              provincia_id : 64,
              minicipio_id : 2601,
              minicipio : "SAN IGNACIO DE SABANETA"
            },
            {
              provincia_id : 64,
              minicipio_id : 2602,
              minicipio : "VILLA LOS ALMACIGOS"
            },
            {
              provincia_id : 64,
              minicipio_id : 2603,
              minicipio : "MONCION"
            },
            {
              provincia_id : 61,
              minicipio_id : 2701,
              minicipio : "MAO"
            },
            {
              provincia_id : 61,
              minicipio_id : 2702,
              minicipio : "ESPERANZA"
            },
            {
              provincia_id : 61,
              minicipio_id : 2703,
              minicipio : "LAGUNA SALADA"
            },
            {
              provincia_id : 42,
              minicipio_id : 2801,
              minicipio : "BONAO"
            },
            {
              provincia_id : 42,
              minicipio_id : 2802,
              minicipio : "MAIMON"
            },
            {
              provincia_id : 42,
              minicipio_id : 2803,
              minicipio : "PIEDRA BLANCA"
            },
            {
              provincia_id : 92,
              minicipio_id : 2901,
              minicipio : "MONTE PLATA"
            },
            {
              provincia_id : 92,
              minicipio_id : 2902,
              minicipio : "BAYAGUANA"
            },
            {
              provincia_id : 92,
              minicipio_id : 2903,
              minicipio : "SABANA GRANDE DE BOYA"
            },
            {
              provincia_id : 92,
              minicipio_id : 2904,
              minicipio : "YAMASA"
            },
            {
              provincia_id : 92,
              minicipio_id : 2905,
              minicipio : "PERALVILLO"
            },
            {
              provincia_id : 25,
              minicipio_id : 3001,
              minicipio : "HATO MAYOR"
            },
            {
              provincia_id : 25,
              minicipio_id : 3002,
              minicipio : "SABANA DE LA MAR"
            },
            {
              provincia_id : 25,
              minicipio_id : 3003,
              minicipio : "EL VALLE"
            },
            {
              provincia_id : 93,
              minicipio_id : 3101,
              minicipio : "SAN JOSE DE OCOA"
            },
            {
              provincia_id : 93,
              minicipio_id : 3102,
              minicipio : "SABANA LARGA"
            },
            {
              provincia_id : 93,
              minicipio_id : 3103,
              minicipio : "RANCHO ARRIBA"
            },
            {
              provincia_id : 1,
              minicipio_id : 3201,
              minicipio : "SANTO DOMINGO ESTE"
            },
            {
              provincia_id : 1,
              minicipio_id : 3202,
              minicipio : "SANTO DOMINGO OESTE"
            },
            {
              provincia_id : 1,
              minicipio_id : 3203,
              minicipio : "SANTO DOMINGO NORTE"
            },
            {
              provincia_id : 1,
              minicipio_id : 3204,
              minicipio : "BOCA CHICA"
            },
            {
              provincia_id : 1,
              minicipio_id : 3205,
              minicipio : "SAN ANTONIO DE GUERRA"
            },
            {
              provincia_id : 1,
              minicipio_id : 3206,
              minicipio : "LOS ALCARRIZOS"
            },
            {
              provincia_id : 1,
              minicipio_id : 3207,
              minicipio : "PEDRO BRAND"
            }
            ]

          let mu = e

          let result = data.filter(x => x.minicipio_id == e)
          const str = result[0]?.minicipio.toLocaleLowerCase();
          const str2 = str?.charAt(0).toUpperCase() + str?.slice(1);
          return str2
        }
    }

 function reside(e) {
      switch(e) {
        case "true":
          return "El ciudadano reside en el país";
          break;
        case "false":
          return "El ciudadano no reside en el país";
          break;
      }
    }

 function printProvincia(e) {

          switch (e) {
            case '1':
              return "Distrito Nacional"
              break;
            case "21":
              return "San Pedro de Macoris"
              break;
            case "22":
              return "La Romana"
              break;
            case "23":
              return "La Altagracia"
              break;
            case "24":
              return "El Seibo"
              break;
            case "25":
              return "Hato Mayor"
              break;
            case "25":
              return "Hato Mayor"
              break;
            case "31":
              return "Duarte"
              break;
            case "32":
              return "Samana"
              break;
            case "33":
              return "Maria Trinidad Sánchez"
              break;
            case "34":
              return "Salcedo"
              break;
            case "41":
              return "La Vega"
              break;
            case "42":
              return "Monseñor Nouel"
              break;
            case "43":
              return "Sánchez Ramirez"
              break;
            case "51":
              return "Santiago"
              break;
            case "56":
              return "Espaillat"
              break;
            case "57":
              return "Puerto Plata"
              break;
            case "61":
              return "Valverde"
              break;
            case "62":
              return "Monte Cristi"
              break;
            case "63":
              return "Dajabón"
              break;
            case "64":
              return "Santiago Rodríguez"
              break;
            case "71":
              return "Azua"
              break;
            case "72":
              return "San Juan de la Maguana"
              break;
            case "73":
              return "Elías Piña"
              break;
            case "81":
              return "Barahona"
              break;
            case "82":
              return "Bahoruco"
              break;
            case "83":
              return "Independencia"
              break;
            case "84":
              return "Pedernales"
              break;
            case "91":
              return "San Cristóbal"
              break;
            case "92":
              return "Monte Plata"
              break;
            case "93":
              return "San José de Ocoa"
              break;
            case "94":
              return "Peravia"
              break;

          }
    }
 function printProvincia2(e, reside) {
 console.log(reside)
        if(reside == 'false') {
          return e;
        } else {
          switch (e) {
            case '1':
              return "Distrito Nacional"
              break;
            case "21":
              return "San Pedro de Macoris"
              break;
            case "22":
              return "La Romana"
              break;
            case "23":
              return "La Altagracia"
              break;
            case "24":
              return "El Seibo"
              break;
            case "25":
              return "Hato Mayor"
              break;
            case "25":
              return "Hato Mayor"
              break;
            case "31":
              return "Duarte"
              break;
            case "32":
              return "Samana"
              break;
            case "33":
              return "Maria Trinidad Sánchez"
              break;
            case "34":
              return "Salcedo"
              break;
            case "41":
              return "La Vega"
              break;
            case "42":
              return "Monseñor Nouel"
              break;
            case "43":
              return "Sánchez Ramirez"
              break;
            case "51":
              return "Santiago"
              break;
            case "56":
              return "Espaillat"
              break;
            case "57":
              return "Puerto Plata"
              break;
            case "61":
              return "Valverde"
              break;
            case "62":
              return "Monte Cristi"
              break;
            case "63":
              return "Dajabón"
              break;
            case "64":
              return "Santiago Rodríguez"
              break;
            case "71":
              return "Azua"
              break;
            case "72":
              return "San Juan de la Maguana"
              break;
            case "73":
              return "Elías Piña"
              break;
            case "81":
              return "Barahona"
              break;
            case "82":
              return "Bahoruco"
              break;
            case "83":
              return "Independencia"
              break;
            case "84":
              return "Pedernales"
              break;
            case "91":
              return "San Cristóbal"
              break;
            case "92":
              return "Monte Plata"
              break;
            case "93":
              return "San José de Ocoa"
              break;
            case "94":
              return "Peravia"
              break;

          }
        }
    }

exports.sendEmail_Pyme = async(req, res) => {
    const {
        email,
        ruta,
        data,
        comentario
    } = req.body

    var transporter = nodemailer.createTransport({
    host: "smtp-mail.outlook.com", // hostname
    secureConnection: true, // TLS requires secureConnection to be false
    port: 587, // port for secure SMTP
    tls: {
       ciphers:'SSLv3'
    },
        auth: {
            user: 'daniel@softnet.com.do',
            pass: 'D@niel1901'
        }
    })

    var mailOptions = {
        from: 'MINISTERIO DE LA JUVENTUD - Incubacion/Aceleraccion',
        to: email,
        subject: `${data.nombre} ${data.apellido} - ${data.cedula} - ${data.nombredelproyecto}`,
        html: `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
</head>
<body>
    <div class="container">
      <h3>Comentarios</h3>
      <p>${comentario}</p>
    </div>
    <br>
        <div>
            <table border="1" style='border-collapse:collapse;'>
                <thead style="border:1px solid #000;">
                    <tr class="table-primary">
                        <td scope="col">Nombre(s)</td>
                        <td scope="col">Apellido(s)</td>
                        <td scope="col">Cédula</td>
                        <td scope="col">Género</td>
                        <td scope="col">Teléfono</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>${ data.nombre }</th>
                        <th>${ data.apellido }</th>
                        <th>${ data.cedula }</th>
                        <th>${ data.genero }</th>
                        <th>${ data.telefono }</th>
                    </tr>
                </tbody>
            </table>


            <table border="1" style='border-collapse:collapse;'>
                <thead>
                    <tr class="table-primary">
                        <td scope="col">Correo electrónico</td>
                        <td scope="col">Nacionalidad</td>
                        <td scope="col">Fecha de nacimiento</td>
                        <td scope="col">Ultimo grado de escolaridad</td>
                        <td scope="col">Institución</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>${ data.email }</th>
                        <th>${ data.nacionalidad }</th>
                        <th>${ data.fechanacimiento }</th>
                        <th>${ data.ultimogrado }</th>
                        <th>${ data.nombreinstitucion }</th>
                    </tr>
                </tbody>
            </table>

            <table border="1" style='border-collapse:collapse;'>
                <thead class="table-primary">
                    <tr>
                        <td scope="col">¿Reside en la República Dominicana?</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>${ reside(data.reside) }</th>
                    </tr>
                </tbody>
            </table>
            <table border="1" style='border-collapse:collapse;'>
                <thead class="table-primary">
                    <tr>
                        <td scope="col">Dirección</td>
                        <td scope="col">Provincia</td>
                        <td scope="col">Municipio</td>
                        <td scope="col">País</td>
                        <td scope="col">Fecha de creación</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>${ data.direccion }</th>
                        <th>${ printProvincia2(data.provincia, data.reside) }</th>
                        <th>${ printMunicipio2(data.municipio, data.reside) }</th>
                        <th>${ data.pais }</th>
                        <th>${ formatDate(data.createdAt) }</th>
                    </tr>
                </tbody>
            </table>

            <table border="1" style='border-collapse:collapse;'>
                <thead>
                    <tr class="table-primary">
                        <td scope="col">Nombre del proyecto</td>
                        <td scope="col">Tipo de proyecto</td>
                        <td scope="col">Sector económico</td>
                        <td scope="col">¿Su formación académica aporta al crecimiento y desarrollo de su idea de negocio o emprendimiento?
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>${ data.nombredelproyecto }</th>
                        <th>${ data.tipoproyecto }</th>
                        <th>${ data.sectoreconomico }</th>
                        <th>${ statusChange(data.questionAcademica) }</th>
                    </tr>
                </tbody>
            </table>


            <table border="1" style='border-collapse:collapse;'>
                <thead>
                    <tr class="table-primary">
                        <td scope="col">Fase Del Proyecto</td>
                        <td scope="col">Objetivo General del Proyecto</td>
                        <td scope="col">Descripción de Proyecto</td>
                        <td scope="col">
                          ¿A qué estracto social de la población impacta el proyecto?
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>${ data.faseDelProyecto }</th>
                        <th>${ data.objetivoGeneral }</th>
                        <th>${ data.descripcionProyecto }</th>
                        <th>${ data.estractoSocial }</th>
                    </tr>
                </tbody>
            </table>

            <table border="1" style='border-collapse:collapse;'>
                <thead>
                    <tr class="table-primary">
                        <td scope="col">Ubicación Geográfica de Ejecución del Proyecto</td>
                        <td scope="col">Tipo de Acompa&ntilde;amiento Solicitado</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>${ data.ubicacionGeografica }</th>
                        <th>${ data.tipoSolicitado }</th>
                    </tr>
                </tbody>
            </table>


            <table border="1" style='border-collapse:collapse;'>
                <thead class="table-primary">
                    <tr>
                        <td>¿Explique cuál es el problema/oportunidad que identificó para crear su idea de negocio?</td>
                        <td scope="col">¿Explique cuál la solución que aporta (producto/servicio o ambos) su idea de negocio al problema identificado?</td>
                        <td scope="col">¿Cómo generaría ingresos su idea de negocio?</td>
                        <td scope="col">Describa cuál sería su mercado objetivo en términos de tamaño, usuarios</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th style="text-align: justify; white-space: pre-line;">${ data.problema_oportunidad }</th>
                        <th style="text-align: justify; white-space: pre-line;">${ data.solucion_aportada }</th>
                        <th style="text-align: justify; white-space: pre-line;">${ data.question_generar_ingresos }</th>
                        <th style="text-align: justify; white-space: pre-line;">${ data.mercado_objetivo }</th>
                    </tr>
                </tbody>
            </table>

            <table border="1" style='border-collapse:collapse;'>
                <thead class="table-primary">
                    <tr>
                        <td scope="col">Estado</td>
                        <td scope="col">Alcance de su mercado</td>
                        <td scope="col">Monto de la inversión</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>${ data.estado }</th>
                        <th>${ data.alcancedemercado }</th>
                        <th>
                            ${ data.monto_inversion }
                        </th>
                    </tr>
                </tbody>
            </table>

            <table border="1" style='border-collapse:collapse;'>
                <thead class="table-primary">
                    <tr>
                        <td scope="col">Provincia del Proyecto</td>
                        <td scope="col">Municipio del Proyecto</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>${ printProvincia(data.provinciaProject) }</th>
                        <th>${ printMunicipio(data.municipioProject) }</th>
                    </tr>
                </tbody>
            </table>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
</body>
</html>`
    }

    transporter.sendMail(mailOptions, function(err, info) {
        if (err) {

            res.status(500).json({ message: 'Error en el intento de enviar mail al destinatario', error: err });
        } else {

            res.status(200).json('Email enviado satisfactoriamente al destinatario')
        }
    })

}

exports.sendEmail_Bank = async(req, res) => {
    const {
        email,
        data,
        comentario
    } = req.body

    var transporter = nodemailer.createTransport({
    host: "smtp-mail.outlook.com", // hostname
    secureConnection: true, // TLS requires secureConnection to be false
    port: 587, // port for secure SMTP
    tls: {
       ciphers:'SSLv3'
    },
        auth: {
            user: 'daniel@softnet.com.do',
            pass: 'D@niel1901'
        }
    })

    var mailOptions = {
        from: 'MINISTERIO DE LA JUVENTUD - Entidad Bancaria',
        to: email,
        subject: `${data.nombre} ${data.apellido} - ${data.cedula} - ${data.nombredelproyecto}`,
        html: `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
</head>
<body>
<div class="container">
<h3>Comentarios</h3>
<p>${comentario}</p>
</div>
    <div>
                <table border="1" style='border-collapse:collapse;'>
                    <thead style="border:1px solid #000;">
                        <tr class="table-primary">
                            <td scope="col">Nombre(s)</td>
                            <td scope="col">Apellido(s)</td>
                            <td scope="col">Cédula</td>
                            <td scope="col">Género</td>
                            <td scope="col">Teléfono</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>${ data.nombre }</th>
                            <th>${ data.apellido }</th>
                            <th>${ data.cedula }</th>
                            <th>${ data.genero }</th>
                            <th>${ data.telefono }</th>
                        </tr>
                    </tbody>
                </table>


                <table border="1" style='border-collapse:collapse;'>
                    <thead>
                        <tr class="table-primary">
                            <td scope="col">Correo electrónico</td>
                            <td scope="col">Nacionalidad</td>
                            <td scope="col">Fecha de nacimiento</td>
                            <td scope="col">Ultimo grado de escolaridad</td>
                            <td scope="col">Institución</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>${ data.email }</th>
                            <th>${ data.nacionalidad }</th>
                            <th>${ data.fechanacimiento }</th>
                            <th>${ data.ultimogrado }</th>
                            <th>${ data.nombreinstitucion }</th>
                        </tr>
                    </tbody>
                </table>

                <table border="1" style='border-collapse:collapse;'>
                    <thead class="table-primary">
                        <tr>
                            <td scope="col">¿Reside en la República Dominicana?</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>${ reside(data.reside) }</th>
                        </tr>
                    </tbody>
                </table>
                <table border="1" style='border-collapse:collapse;'>
                    <thead class="table-primary">
                        <tr>
                            <td scope="col">Dirección</td>
                            <td scope="col">Provincia</td>
                            <td scope="col">Municipio</td>
                            <td scope="col">País</td>
                            <td scope="col">Fecha de creación</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>${ data.direccion }</th>
                            <th>${ printProvincia2(data.provincia, data.reside) }</th>
                            <th>${ printMunicipio2(data.municipio, data.reside) }</th>
                            <th>${ data.pais }</th>
                            <th>${ formatDate(data.createdAt) }</th>
                        </tr>
                    </tbody>
                </table>

                <table border="1" style='border-collapse:collapse;'>
                    <thead>
                        <tr class="table-primary">
                            <td scope="col">Nombre del proyecto</td>
                            <td scope="col">Tipo de proyecto</td>
                            <td scope="col">Sector económico</td>
                            <td scope="col">¿Su formación académica aporta al crecimiento y desarrollo de su idea de negocio o emprendimiento?
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>${ data.nombredelproyecto }</th>
                            <th>${ data.tipoproyecto }</th>
                            <th>${ data.sectoreconomico }</th>
                            <th>${ statusChange(data.questionAcademica) }</th>
                        </tr>
                    </tbody>
                </table>


                <table border="1" style='border-collapse:collapse;'>
                    <thead>
                        <tr class="table-primary">
                            <td scope="col">Fase Del Proyecto</td>
                            <td scope="col">Objetivo General del Proyecto</td>
                            <td scope="col">Descripción de Proyecto</td>
                            <td scope="col">
                              ¿A qué estracto social de la población impacta el proyecto?
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>${ data.faseDelProyecto }</th>
                            <th>${ data.objetivoGeneral }</th>
                            <th>${ data.descripcionProyecto }</th>
                            <th>${ data.estractoSocial }</th>
                        </tr>
                    </tbody>
                </table>

                <table border="1" style='border-collapse:collapse;'>
                    <thead>
                        <tr class="table-primary">
                            <td scope="col">Ubicación Geográfica de Ejecución del Proyecto</td>
                            <td scope="col">Tipo de Acompa&ntilde;amiento Solicitado</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>${ data.ubicacionGeografica }</th>
                            <th>${ data.tipoSolicitado }</th>
                        </tr>
                    </tbody>
                </table>


                <table border="1" style='border-collapse:collapse;'>
                    <thead class="table-primary">
                        <tr>
                            <td>¿Explique cuál es el problema/oportunidad que identificó para crear su idea de negocio?</td>
                            <td scope="col">¿Explique cuál la solución que aporta (producto/servicio o ambos) su idea de negocio al problema identificado?</td>
                            <td scope="col">¿Cómo generaría ingresos su idea de negocio?</td>
                            <td scope="col">Describa cuál sería su mercado objetivo en términos de tamaño, usuarios</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th style="text-align: justify; white-space: pre-line;">${ data.problema_oportunidad }</th>
                            <th style="text-align: justify; white-space: pre-line;">${ data.solucion_aportada }</th>
                            <th style="text-align: justify; white-space: pre-line;">${ data.question_generar_ingresos }</th>
                            <th style="text-align: justify; white-space: pre-line;">${ data.mercado_objetivo }</th>
                        </tr>
                    </tbody>
                </table>

                <table border="1" style='border-collapse:collapse;'>
                    <thead class="table-primary">
                        <tr>
                            <td scope="col">Estado</td>
                            <td scope="col">Alcance de su mercado</td>
                            <td scope="col">Monto de la inversión</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>${ data.estado }</th>
                            <th>${ data.alcancedemercado }</th>
                            <th>
                                ${ data.monto_inversion }
                            </th>
                        </tr>
                    </tbody>
                </table>

                <table border="1" style='border-collapse:collapse;'>
                    <thead class="table-primary">
                        <tr>
                            <td scope="col">Provincia del Proyecto</td>
                            <td scope="col">Municipio del Proyecto</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>${ printProvincia(data.provinciaProject) }</th>
                            <th>${ printMunicipio(data.municipioProject) }</th>
                        </tr>
                    </tbody>
                </table>
            </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
</body>
</html>`
    }

    transporter.sendMail(mailOptions, function(err, info) {
        if (err) {

            res.status(500).json('Error en el intento de enviar mail al destinatario');
        } else {

            res.status(200).json('Email enviado satisfactoriamente al destinatario')
        }
    })

}