const { errorMonitor } = require('events');
const datos = require('../model/datos')
const cron = require('node-cron');
const moment = require('moment')

const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

function generateString(length) {
    let result = '';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

// Test


exports.datos_generales_proyectos = async(req, res) => {
    const {
        nombre,
        apellido,
        cedula,
        genero,
        email,
        telefono,
        nacionalidad,
        fechanacimiento,
        ultimogrado,
        nombreinstitucion,
        direccion,
        provincia,
        municipio,
        pais,
        no_solicitud,
        completado,
        fase_static,
        provinciaProject,
        municipioProject,
        reside,

        faseDelProyecto,
        objetivoGeneral,
        descripcionProyecto,
        estractoSocial,
        ubicacionGeografica,
        tipoSolicitado,

        acceso_internet,
        redes_sociales_confirm,
        redes_sociales,
        capacitacion,
        dias_capacitacion,
        acceso_internet_confirm,
        temas_relacionados,
        empresa_constituida,
        rnc,
        financiado_confirm,
        financiado,
        emp_cerca,
  estracto,
  ubicacion,
  tiempo_emp,
  empleos,
  prestamo
    } = req.body;

    try {
        const response = await datos.create({
            nombre,
            apellido,
            cedula,
            genero,
            email,
            telefono,
            nacionalidad,
            fechanacimiento,
            ultimogrado,
            nombreinstitucion,
            direccion,
            provincia,
            municipio,
            pais,
            no_solicitud: generateString(5),
            completado,
            fase_static,
            provinciaProject,
            municipioProject,
            reside,
            faseDelProyecto,
            objetivoGeneral,
            descripcionProyecto,
            estractoSocial,
            ubicacionGeografica,
            tipoSolicitado,
            acceso_internet,
            redes_sociales_confirm,
            redes_sociales,
            capacitacion,
            dias_capacitacion,
            acceso_internet_confirm,
            temas_relacionados,
            empresa_constituida,
            rnc,
            financiado_confirm,
            financiado,
            emp_cerca,
            estracto,
            ubicacion,
            tiempo_emp,
            empleos,
            prestamo
        })
        res.status(200).json({ data: 'Guardado Correctamente', solicitud: response.no_solicitud, id: response._id })

    } catch (err) {
        return res.status(400).json({
            data: err
        })
    }
}

exports.ver_datos_generales = async(req, res) => {
    console.log('paso')
    datos.find({})
    .populate('banco')
    .populate('pyme')
    .exec((err, data) => {
        if (err) {
            res.status(400).json(err)
        } else {
            res.status(200).json(data)
        }
    })
}

// cron.schedule('* * * * * *', async() => {
//     const data = await datos.find({});
//     data.forEach(async(elemento) => {

//         const fase = parseInt(elemento.fase);
//         let fecha = new Date();
//         //Obtiene la diferencia de los dias entre las dos fechas
//         const diasDif = fecha.getTime() - elemento.updatedAt.getTime();
//         let diasTrans = Math.abs(Math.round(diasDif / (1000 * 60 * 60 * 24)));

//         //Minutos - eliminar
//         let minDif = Math.round(((diasDif % 86400000) % 3600000) / 60000);
//         //Eliminar
//         diasTrans = minDif;

//         if ((fase === 7 && diasTrans >= 10) || (fase === 8 && diasTrans >= 10)) {

//             await datos.findByIdAndUpdate({ _id: elemento._id }, { fase: 6, disableVice: false })
//         }
//     })
// });

exports.ver_datos_generales_2 = async(req, res) => {
    datos.find(req.params.analista)
        .then(function(data) {
            const page = parseInt(req.query.page);
            const limit = req.query.limit;

            const startIndex = (page - 1) * limit;
            const endIndex = page * limit;
            const results = {}

            if (endIndex < data.length) {
                results.next = {
                    page: page + 1,
                    limit: limit
                }
            }

            if (startIndex > 0) {
                results.previous = {
                    page: page - 1,
                    limit: limit
                }
            }


            results.results = data.slice(startIndex, endIndex)
            res.status(200).json(results)
        })
}

exports.find_query = async(req, res) => {
    datos.find(req.query)
        .then(data => {

            res.json(data)
        })
}

exports.ver_datos_registro = async(req, res) => {
    datos.findById(req.params)
    .populate([
        {
            path: 'banco',
            model: 'BancosSchema'
        },
        {
            path: 'pyme',
            model: 'PymeSchema'
        }
    ])
        .then(function(data) {

            res.status(200).json(data)
        })
}

exports.search_general_registros = async(req, res) => {
    datos.findOne(req.query)
        .then(function(data) {

            res.status(200).json(data)
        })
}

exports.findSearch = async(req, res) => {
    datos.find(req.query)
        .then(data => {
            res.status(200).json(data)
        })
        .catch((err) => {
            res.status(400).json(err)
            throw err;
        })
}

exports.next_register = async(req, res) => {
    datos.findByIdAndUpdate({ _id: req.params._id }, req.body)
        .then(function() {
            datos.findOne({ _id: req.params._id })
                .then(function(data) {

                    res.status(200).json(data)
                })
        })
}

exports.update_general_registro = async(req, res) => {
    datos.findByIdAndUpdate({ _id: req.params._id }, req.body)
        .then(function() {
            datos.findOne({ _id: req.params._id })
                .then(function(data) {

                    res.status(200).json(data)
                })
        })
}

exports.count_general_registros = async(req, res) => {
    datos.countDocuments({}),
        function(err, count) {

            res.status(200).json({ data: Number(count) })
        }
}

exports.delete_registro = async(req, res) => {
    datos.findByIdAndRemove({ _id: req.params._id })
        .then(function(data) {
            res.status(200).json(data)
        })
}

exports.update_registro = async(req, res) => {
    datos.findByIdAndUpdate({ _id: req.params._id }, req.body)
        .then(function() {
            User.findOne({ _id: req.params._id })
                .then(function(data) {
                    res.status(200).json(data)
                })
        })
}

exports.continuar_apply = async(req, res) => {
    datos.findOne(req.query)
        .then(data => {
            res.status(200).json(data)

        })
        .catch((err) => {
            res.status(400).json(err)

        })
}

exports.count_analista = async(req, res) => {
    datos.find(req.query)
        .then(data => {
            res.status(200).json(data)

        })
        .catch((err) => {
            res.status(400).json(err)

        })
}

exports.findProjects = async(req, res) => {
    var regex = new RegExp(req.params.nombredelproyecto, "i"),
        query = { nombredelproyecto: regex };

    datos.find(query, function(err, data) {
        if (err) {
            res.json(err);
        }

        res.json(data);
    });
}

exports.findProjectsCedula = async(req, res) => {
    var regex = new RegExp(req.params.cedula, "i"),
        query = { cedula: regex };

    datos.find(query, function(err, data) {
        if (err) {
            res.json(err);
        }

        res.json(data);
    });
}

exports.findProjectsCode = async(req, res) => {
    var regex = new RegExp(req.params.no_solicitud, "i"),
        query = { no_solicitud: regex };

    datos.find(query, function(err, data) {
        if (err) {
            res.json(err);
        }

        res.json(data);
    });
}

exports.uploadFiles = async(req, res) => {
    datos.findOneAndUpdate({ _id: req.params._id }, {
            $push: {
                files: req.body.files
            }
        })
        .then(data => {
            res.status(200).json("Success")
        })
        .catch((err) => {
            res.status(500).json(err)
        })
}
exports.uploadSolicitados = async(req, res) => {
    datos.findOneAndUpdate({ _id: req.params._id }, {
            $push: {
                solicitados_files: req.body.solicitados
            }
        })
        .then(data => {
            res.status(200).json("Success")
        })
        .catch((err) => {
            res.status(500).json(err)
        })
}

exports.uploadReportAnalist = async(req, res) => {
    datos.findOneAndUpdate({ _id: req.params._id }, {
            $push: {
                report_analist: req.body.report_analist
            }
        })
        .then(data => {
            res.status(200).json("Success")
        })
        .catch((err) => {
            res.status(500).json(err)
        })
}

exports.uploadPropuesta = async(req, res) => {
    datos.findOneAndUpdate({ _id: req.params._id }, {
            $push: {
                propuesta_proyecto: req.body.propuesta
            }
        })
        .then(data => {
            res.status(200).json("Success")
        })
        .catch((err) => {
            res.status(500).json(err)
        })
}

exports.uploadDevolucion = async(req, res) => {
    datos.findOneAndUpdate({ _id: req.params._id }, {
            $push: {
                filesDirector: req.body.filesDirector
            }
        })
        .then(data => {
            res.status(200).json("Success")
        })
        .catch((err) => {
            res.status(500).json(err)
        })
}

exports.incompletos = async(req, res) => {
    const pageNumber = Number(req.query.offset)
    const pageLimit = Number(req.query.limit)
    datos.paginate({ completado: false }, { page: pageNumber, limit: pageLimit }, (err, result) => {
        res.status(200).json({
            data: result.docs,
            totalPage: result.pages - Number(result.page),
            limit: result.limit,
            offset: result.page
        })
    })
}

exports.reportFilter = async(req, res) => {
    const pageNumber = Number(req.query.offset)
    const pageLimit = Number(req.query.limit)
    const query = req.body
    datos.paginate(
        query, {
            page: pageNumber,
            limit: pageLimit
        }, (err, data) => {
            res.status(200).json({
                data: data.docs,
                totalPage: data.pages - Number(data.page),
                limit: data.limit,
                offset: data.page,
                url: query
            })
        }
    )
}

exports.reportAll = async(req, res) => {
    const pageNumber = Number(req.query.offset)
    const pageLimit = Number(req.query.limit)
    datos.paginate({ pais: 'Republica Dominicana' }, {
            page: pageNumber,
            limit: pageLimit
        },
        (err, result) => {
            res.status(200).json({
                data: result.docs,
                totalPage: result.pages - Number(result.page),
                limit: result.limit,
                offset: result.page
            })
        }
    )
}

exports.filterFind = async(req, res) => {
    try {
        const fechaInicial = req.body.fechaInicial;
        const fechaFinal = req.body.fechaFinal;
        const provincia = req.body.provincia;
        const prioridad = req.body.prioridad;
        const fase = req.body.fase;

        var queryCond = {};
        if (fechaInicial && fechaFinal) {
            queryCond.createdAt = {
                $gte: fechaInicial,
                $lt: fechaFinal
            }
        }
        if (provincia) {
            queryCond.provincia = provincia
        }
        if (prioridad) {
            queryCond.prioridad = prioridad
        }
        if (fase) {
            queryCond.fase = fase
        }
        const filter = await datos.find(queryCond, (err, result) => {
            if (err) {
                res.status(400).send({
                    message: "El registro no existe"
                })
            }
            res.status(200).send(result);
        })
    } catch (err) {
        res.status(500).send({
            message: 'Ocurrio un error'
        })

    }
}

exports.deleteFile = async(req, res) => {
    datos.updateOne({
            _id: req.params.id
        }, {
            $pull: {
                files: {
                    id: req.body.key
                }
            }
        })
        .then(res => {
            res.json(res)
        })
        .catch((err) => {
            res.json(err)
        })
}

exports.deleteReport = async(req, res) => {
    datos.updateOne({
        _id: req.params.id
    },
    {
        $pull: {
            report_analist: {
                id: req.body.key
            }
        }
    })
    .then(res => {
        res.json(res);
    })
    .catch((err) => {
        res.json(err);
    })
}

exports.deletePropuesta = async(req, res) => {
    datos.updateOne({
        _id: req.params.id
    },
    {
        $pull: {
            propuesta_proyecto: {
                id: req.body.key
            }
        }
    })
    .then(res => {
        res.json(res);
    })
    .catch((err) => {
        res.json(err);
    })
}

exports.deleteMotivo = async(req, res) => {
    datos.updateOne({
        _id: req.params.id
    },
    {
        $pull: {
            filesDirector: {
                id: req.body.key
            }
        }
    })
    .then(res => {
        res.json(res);
    })
    .catch((err) => {
        res.json(err);
    })
}