const projects = require('../model/datos');
const pdf = require('html-pdf');

exports.generateCharts = async(req, res) => {
    var pCompletados = ''
    var pNoCompletados = ''
    var rechazados = ''
    var inProgess = ''
    var final = ''

    projects.find({ completado: true })
        .then(data => {
            pCompletados = data.length

        })
    projects.find({ completado: false })
        .then(data => {
            pNoCompletados = data.length

        })
    projects.find({ fase: '0' })
        .then(data => {
            rechazados = data.length

        })
    projects.find({ fase: '8' })
        .then(data => {
            inProgess = data.length

        })
    projects.find({ fase: '9' })
        .then(data => {
            final = data.length

        })

    setTimeout(() => res.status(200).json({
        recibidas: pCompletados,
        noCompletados: pNoCompletados,
        reject: rechazados,
        enProgreso: inProgess,
        finalizados: final
    }), 2000)
}

exports.generatePDFReport = async(req, res) => {
    var pCompletados = ''
    var pNoCompletados = ''
    var rechazados = ''
    var inProgess = ''
    var final = ''

    projects.find({ completado: true })
        .then(data => {
            pCompletados = data.length
        })
    projects.find({ completado: false })
        .then(data => {
            pNoCompletados = data.length
        })
    projects.find({ fase: '0' })
        .then(data => {
            rechazados = data.length
        })
    projects.find({ fase: '6' })
        .then(data => {
            inProgess = data.length
        })
    projects.find({ fase: '7' })
        .then(data => {
            final = data.length
        })


    const content = `
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reporte</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>

        <header style="padding: 10px;">
            <img src="./imgReport/Logo centrado-color.png" style="width: 15%; display: block; margin-left: auto; margin-right: auto;" alt="">
            <h1 style="text-align: center;">Reporte de Proyectos</h1>
            <p style="text-align: center;">Generado via Plataforma de Emprendimiento</p>
            <hr>
        </header>

        <div style="padding: 10px;">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Solicitudes Recibidas</th>
                                    <th scope="col">Solicitudes Incompletas</th>
                                    <th scope="col">Solicitudes Rechazadas</th>
                                    <th scope="col">Solicitudes En Progreso</th>
                                    <th scope="col">Solicitudes Finalizadas</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
        </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
        crossorigin="anonymous"></script>

</body>
</html>
        `

    pdf.create(content).toFile('./report.pdf', function(err, res) {
        if (err) {

        } else {

        }
    });
    return res.json('Exitos')
}