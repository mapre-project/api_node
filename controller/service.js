const image = require('../model/image.js')

exports.updateData = async (req, res) => {
    const {
        impacto,
        ministerio,
        oracion,
        pastor1,
        pastor2
    } = req.body;

    try {
        const response = await image.create({
        impacto,
        ministerio,
        oracion,
        pastor1,
        pastor2 
        })
        res.status(200).json(response)
    } catch (err) {
        return res.status(400).json(err)
    }
}

exports.update = async (req, res) => {
        image.findByIdAndUpdate({ _id: req.params._id }, req.body)
        .then(function () {
            image.findOne({ _id: req.params._id })
                .then(function (data) {
                    res.status(200).json(data)
            })
    })
}

exports.view = async (req, res) => {
    image.find({})
        .then(function (data) {
            res.status(200).json(data)
    })
}