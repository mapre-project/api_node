const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const User = require("../model/user");
const Datos = require("../model/datos");

const JWT_SECRET =
  "ssafarq34aksbdfoib2o3ufoqwbqwrfo*)&(ˆ*&&ˆ**&ˆ*kjnskhfkjsfaisdf";

const generate = (email) => {
  const date = new Date();
  date.setHours(date.getHours() + 24);
  return jwt.sign({ email, expiration: date }, JWT_SECRET);
};

function validateRol(rolRegister) {
  return new Promise((resolve, reject) => {
    if (
      rolRegister == "Viceministro" ||
      rolRegister == "Ministro Administrativo" ||
      rolRegister == "Director"
    ) {
      User.find({ rol: rolRegister }).exec((err, result) => {
        if (result.length == 0 || result == []) {
          resolve("Registrar");
        } else {
          reject("No registrar");
        }
      });
    } else {
      resolve();
    }
  });
}

exports.Register = async (req, res) => {
  try {
    validateRol(req.body.rol)
    .then(async (result) => {
      const {
        username,
        password: plainTextPassword,
        nombre,
        rol,
        alias,
        departamento,
        biografia,
        fechaDeRegistro,
        imgPerfil,
      } = req.body;

      if (!username || typeof username !== "string") {
        return res
          .status(400)
          .json({ status: "error", error: "Usuario Invalido" });
      }

      if (!plainTextPassword || typeof plainTextPassword !== "string") {
        return res
          .status(400)
          .json({ status: "error", error: "Password Invalido" });
      }

      if (plainTextPassword.length < 6) {
        return res.status(400).json({
          status: "error",
          error:
            "Password inseguro, su password debe contener mas de 6 caracteres",
        });
      }

      const password = await bcrypt.hash(plainTextPassword, 10);

      try {
        const response = await User.create({
          username,
          password,
          nombre,
          rol,
          alias,
          departamento,
          biografia,
          fechaDeRegistro,
          imgPerfil,
        });
      } catch (error) {
        if (error.code === 11000) {
          return res.status(400).json({
            status: "error",
            error: "ESTE USUARIO YA SE ENCUENTRA REGISTRADO",
          });
        }
        throw error;
      }

      res
        .status(200)
        .json({ msg: "Usuario creado correctamente", status: 200 });
    })
    .catch((err) => {
      return res
        .status(400)
        .json(
          "YA SE HA ALCANZADO EL LIMITE DE USUARIOS PERMITIDOS CON ESTE ROL"
        );
    });
  } catch(err) {
    return res.status(500).json({
      ok: false,
      message: "Error inesperado",
      error: err
    })
  }
};

exports.Login = async (req, res) => {
  const { username, password } = req.body;
  const user = await User.findOne({ username }).lean();

  if (!user) {
    return res.status(400).json({ error: "Usuario/Contraseña incorrectos" });
  }

  if (await bcrypt.compare(password, user.password)) {
    const token = jwt.sign(
      {
        id: user._id,
        username: user.username,
      },
      JWT_SECRET,
      {
        expiresIn: "1h",
      }
    );
    return res.status(200).json({
      status: 200,
      message: "Inicio de sesión exitoso",
      jwt: token,
      username: user.username,
      user: user.nombre + " " + user.apellido,
      rol: user.rol,
      id: user._id,
      img_perfil: user.imgPerfil,
    });
  }

  res.status(400).json({ error: "Usuario o Contraseña Incorrecto" });
};

exports.view_user = async (req, res) => {
  User.find({}).then(function (data) {
    res.status(200).json(data);
  });
};

exports.view_user_id = async (req, res) => {
  User.findById(req.params).then(function (data) {
    res.status(200).json(data);
  });
};

exports.delete_user = async (req, res) => {
  User.findByIdAndRemove({ _id: req.params._id }).then(function (data) {
    res.status(200).json(data);
  });
};

exports.update_user = async (req, res) => {
  User.findByIdAndUpdate({ _id: req.params._id }, req.body).then(function () {
    User.findOne({ _id: req.params._id }).then(function (data) {
      res.status(200).json(data);
    });
  });
};

exports.find_for_query = async (req, res) => {
  User.findOne(req.query).then(function (data) {
    res.status(200).json(data);
  });
};

exports.find_for_details = async (req, res) => {
  Datos.findOne(req.query).then(function (data) {
    res.status(200).json(data);
  });
};

exports.find_analist_email = async (req, res) => {
  var regex = new RegExp(req.params.username, "i"),
    query = { username: regex };

  User.find(query, function (err, user) {
    if (err) {
      res.json(err);
    }

    res.json(user);
  });
};

exports.findSearch = async (req, res) => {
  User.find(req.query)
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) => {
      res.status(400).json(err);
      throw err;
    });
};

exports.find_analist_name = async (req, res) => {
  var regex = new RegExp(req.params.nombre, "i"),
    query = { nombre: regex };

  User.find(query, function (err, user) {
    if (err) {
      res.json(err);
    }

    res.json(user);
  });
};

// exports.change_password = async(req, res) => {
//     const { username, newpassword: plainTextPassword } = req.body

//     if (!plainTextPassword || typeof plainTextPassword !== 'string') {
//         return res.json({ status: 'error', error: 'Contraseña Incorrecta' })
//     }

//     if (plainTextPassword.length < 6) {
//         return res.json({ status: 'error', error: 'Password too small. Should be atleast 6 characters' })
//     }

//     try {
//         const password = await bcrypt.hash(plainTextPassword, 10)
//         const _id = username
//         await User.updateOne({ _id }, {
//             $set: { password }
//         })
//         res.json({ status: 'Contraseña Actualizada' })
//     } catch (err) {
//         res.json({ status: 'error', error: ';))' })
//     }
// }

exports.updatePassword = async (req, res) => {
  try {
    const body = req.body;
    const user = body.username;
    const pass = body.newpassword;
    const passwordNew = await bcrypt.hash(pass, 10);

    User.find({ username: user }).then(async (data) => {
      await User.findByIdAndUpdate(
        { _id: data[0]._id },
        {
          password: passwordNew,
        },
        (err, response) => {
          if (err) {
            res.status(500).json(err);
          }
          res.status(200).json("Contraseña actualizada");
        }
      );
    });
  } catch (err) {
    res.json(err);
  }
};

exports.findAnalist = async (req, res) => {
  const analist = await User.find({ rol: "Analista" });
  res.status(200).json(analist);
};

exports.restorePasswordMail = async (req, res) => {
  const user = await User.findOne({ username: req.body.user });
  if (!user) {
    res.status(400).json("El usuario proporcionado no existe.");
  } else {
    const token = generate(req.body.user);

    var transporter = nodemailer.createTransport({
    host: "smtp-mail.outlook.com", // hostname
    secureConnection: true, // TLS requires secureConnection to be false
    port: 587, // port for secure SMTP
    tls: {
       ciphers:'SSLv3'
    },
      auth: {
        user: "laboratorio@juventud.gob.do",
        pass: "@UAhMT:.?QS3.?3",
      },
    });
    //
    // res.json(user._id)
    var mailOptions = {
      from: "MINISTERIO DE LA JUVENTUD",
      to: req.body.user,
      subject: `Recuperar Contraseña`,
      html: `<!DOCTYPE html>
                <html lang="en">
                
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Document</title>
                </head>
                
                <body>
                    <h1>Recuperar Contraseña</h1>
                    <br>
                    <p>Estimad@, por medio de este correo podra cambiar su contraseña, haga click en el siguiente enlace para poder hacer el reseteo de contraseña: <a href="http://proyecto-dev.s3-website-us-east-1.amazonaws.com/reset-password/${user._id}/${token}">Recuperar Contraseña</a> </p>
                    <br>
                </body>
                
                </html>`,
    };

    transporter.sendMail(mailOptions, function (err, info) {
      if (err) {
        res
          .status(500)
          .json("Error en el intento de enviar mail al destinatario");
      } else {
        res.status(200).json({
          msg: "Email enviado satisfactoriamente al destinatario",
          data: info,
        });
      }
    });
  }
};
