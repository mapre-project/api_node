const comments = require('../model/comments')
const reply = require('../model/comments_reply')

exports.comments_post = async(req, res) => {
    const {
        id,
        user,
        comment,
        userView
    } = req.body;

    try {
        const response = await comments.create({
            id,
            user,
            comment,
            userView
        })
        res.status(200).json('Comentario Agregado')
    } catch (err) {
        return res.status(400).json({
            err
        })
    }
}

exports.updateComments = async(req, res) => {
    comments.findByIdAndUpdate({ _id: req.params._id }, req.body)
        .then(function() {
            comments.findOne({ _id: req.params._id })
                .then(function(data) {

                    res.status(200).json(data)
                })
        })
}

exports.reply_post = async(req, res) => {
    const {
        id_comment,
        user_reply,
        comment,
        userView
    } = req.body;

    try {
        const response = await reply.create({
            id_comment,
            user_reply,
            comment,
            userView
        })
        res.status(200).json('Reply Agregado')

    } catch (err) {
        return res.status(400).json(err)

    }
}

exports.reply = async(req, res) => {
    reply.find(req.query)
        .then(data => {
            res.status(200).json(data)

        })
        .catch((err) => {
            res.status(400).json(err)

        })
}

exports.commentsOne = async(req, res) => {
    let limit = 5
    comments.find(req.query)
        .limit(limit)
        .then(data => {
            res.status(200).json([data])
        })
}

exports.comments = async(req, res) => {
    comments.find(req.query)
        .then(data => {
            res.status(200).json(data)
        })
}