const bankModel = require('../model/banks.js');
const pymesModel = require('../model/pymes.js');

exports.createBank = async(req, res) => {
    try {
        const body = req.body;

        const existBank = await bankModel.findOne({ emailBanco: body.emailBanco });

        if (existBank) return res.status(400).json({
            ok: false,
            message: 'El correo electrónico ya se encuentra registrado en el sistema.'
        })

        const bank = new bankModel(body);
        await bank.save((err, bank) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            return res.status(200).json({
                ok: true,
                message: 'Banco creado correctamente',
                bank
            });
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            message: 'Error inesperado del sistema, por favor contacte al administrador del sistema.',
            error: error
        })
    }
}

exports.getBanks = async(req, res) => {
    try {
        const banks = await bankModel.find({});
        return res.status(200).json({
            ok: true,
            data: banks
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            message: 'Error inesperado del sistema, por favor contacte al administrador del sistema.',
            error: error
        });
    }
}

exports.getBank = async(req, res) => {
    try {
        const id = req.params.id;
        const bank = await bankModel.findById(id);
        return res.status(200).json({
            ok: true,
            data: bank
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            message: 'Error inesperado del sistema, por favor contacte al administrador del sistema.',
            error: error
        });
    }
}

exports.updateBank = async(req, res) => {
    try {
        const id = req.params.id;
        const body = req.body;
        const bank = await bankModel.findByIdAndUpdate(id, body, { new: true });
        return res.status(200).json({
            ok: true,
            message: 'Banco actualizado correctamente',
            data: bank
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            message: 'Error inesperado del sistema, por favor contacte al administrador del sistema.',
            error: error
        });
    }
}

exports.createPymes = async(req, res) => {
    try {
        const body = req.body;

        const existPyme = await pymesModel.findOne({ emailPyme: body.emailPyme });

        if (existPyme) return res.status(400).json({
            ok: false,
            message: 'El correo electrónico ya se encuentra registrado en el sistema.'
        });

        const pymes = new pymesModel(body);
        await pymes.save((err, pymes) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            return res.status(200).json({
                ok: true,
                message: 'PYME creado correctamente',
                data: pymes
            });
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            message: 'Error inesperado del sistema, por favor contacte al administrador del sistema.',
            error: error
        })
    }
}

exports.getPymes = async(req, res) => {
    try {
        const pymes = await pymesModel.find({});
        return res.status(200).json({
            ok: true,
            data: pymes
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            message: 'Error inesperado del sistema, por favor contacte al administrador del sistema.',
            error: error
        });
    }
}

exports.getPyme = async(req, res) => {
    try {
        const id = req.params.id;
        const pymes = await pymesModel.findById(id);
        return res.status(200).json({
            ok: true,
            data: pymes
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            message: 'Error inesperado del sistema, por favor contacte al administrador del sistema.',
            error: error
        });
    }
}

exports.updatePyme = async(req, res) => {
    try {
        const id = req.params.id;
        const body = req.body;
        const pymes = await pymesModel.findByIdAndUpdate(id, body, { new: true });
        return res.status(200).json({
            ok: true,
            message: 'PYME actualizado correctamente',
            data: pymes
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            message: 'Error inesperado del sistema, por favor contacte al administrador del sistema.',
            error: error
        });
    }
}