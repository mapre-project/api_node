const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate')
const moment = require('moment');

const DatosSchema = new mongoose.Schema({
        nombre: String,
        apellido: String,
        cedula: String,
        genero: String,
        email: String,
        telefono: String,
        nacionalidad: String,
        fechanacimiento: String,
        ultimogrado: String,
        nombreinstitucion: String,
        direccion: String,
        provincia: String,
        municipio: String,
        pais: String,
        nombredelproyecto: String,
        tipoproyecto: String,
        sectoreconomico: String,
        questionAcademica: String,
        estado: String,
        alcancedemercado: String,
        problema_oportunidad: String,
        solucion_aportada: String,
        question_generar_ingresos: String,
        mercado_objetivo: String,
        question_sus3_ventajas: String,
        question_sus3_dificultades: String,
        negocio_a_5_anos: String,
        novedoso_de_su_idea: String,
        monto_inversion: String,
        solucion: String,
        dificultades: String,
        no_solicitud: String,
        prioridad: String,
        disableVice: {
            type: Boolean,
            default: true
        },
        files: [Object],
        solicitados_files: [Object],
        report_analist: [Object],
        propuesta_proyecto: [Object],
        devolucionPerfil: [Object],
        devolucionPropuesta: [Object],
        filesDirector: [Object],
        completado: Boolean,
        fase: String,
        fecha: {
            type: String,
            default: moment().format('DD/MM/YYYY')
        },
        analista: String,
        nombre_analista: String,
        status: String,
        fase_static: String,
        director: String,
        ministro: String,
        viceministro: String,
        provinciaProject: String,
        municipioProject: String,
        reside: String,

        faseDelProyecto: String,
        objetivoGeneral: String,
        descripcionProyecto: String,
        estractoSocial: String,
        ubicacionGeografica: String,
        tipoSolicitado: String,
        acceso_internet: String,
        redes_sociales_confirm: Boolean,
        redes_sociales: String,
        capacitacion: Object,
        dias_capacitacion: Object,
        acceso_internet_confirm: Boolean,
        temas_relacionados: Object,
        empresa_constituida: String,
        rnc: String,
        financiado_confirm: Boolean,
        financiado: String,
        banco: { type: mongoose.Schema.Types.ObjectId, ref: 'BancosSchema' },
        pyme: { type: mongoose.Schema.Types.ObjectId, ref: 'PymeSchema' },

        emp_cerca: String,
        estracto: String,
        ubicacion: String,
        tiempo_emp:String,
        empleos: String,
        prestamo: String
    },

    {
        collection: 'datos',
        timestamps: true
    }

);
DatosSchema.plugin(mongoosePaginate);
const model = mongoose.model('DatosSchema', DatosSchema)
module.exports = model;