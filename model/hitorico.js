const mongoose = require('mongoose');

const HistoricoSchema = new mongoose.Schema({
    idSolicitud: String,
    accion: String,
    fase: String,
    usuario: String,
    fecha: {
        type: Date,
        default: Date.now
    }
},

    { collection: 'historico' }

)

const model = mongoose.model('HistoricoSchema', HistoricoSchema);
module.exports = model;