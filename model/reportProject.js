const mongoose = require('mongoose');

const ReportProjectSchema = new mongoose.Schema({
    idSolicitud: String,
    idUser: String,
    title: String,
    fase: String
},

    { collection: 'report_project' }

)

const model = mongoose.model('ReportProjectSchema', ReportProjectSchema);
module.exports = model;