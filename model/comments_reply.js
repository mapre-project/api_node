const mongoose = require('mongoose');

const CommentsReplySchema = new mongoose.Schema({
        id_comment: String,
        user_reply: String,
        comment: String,
        userView: Boolean
    },

    {
        collection: 'comments_reply',
        timestamps: true
    }

)

const model = mongoose.model('CommentsReplySchema', CommentsReplySchema);
module.exports = model;