const mongoose = require('mongoose');

const ImageSchema = new mongoose.Schema({
    impacto: String,
    ministerio: String,
    oracion: String,
    pastor1: String,
    pastor2: String
},

{ collection: 'image' }
    
)

const model = mongoose.model('ImageSchema', ImageSchema)
module.exports = model;