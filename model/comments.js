const mongoose = require('mongoose');

const CommentsSchema = new mongoose.Schema({
    id: String,
    user: String,
    comment: String,
    reply: Boolean,
    userView: Boolean
},

    {
        collection: 'comments',
        timestamps: true
    }

)

const model = mongoose.model('CommentsSchema', CommentsSchema);
module.exports = model;