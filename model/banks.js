const mongoose = require('mongoose');

const BancosSchema = new mongoose.Schema({
    nombreBanco: {
        type: String
    },
    nombreContacto: {
        type: String
    },
    direccionBanco: {
        type: String
    },
    emailBanco: {
        type: String
    },
    telefonoBanco: {
        type: String
    },
    comentarios: {
        type: String
    }
},

    { collection: 'bancos' }

)

const model = mongoose.model('BancosSchema', BancosSchema);
module.exports = model;