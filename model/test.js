const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate')
const moment = require('moment');

const FilesSchema = new mongoose.Schema({
    name: String,
    filesPropuesta: [
        {
            propuesta: [Object],
            devolucion: [Object]
        }
    ]
    },

    {
        collection: 'filesTest',
        timestamps: true
    }

);
const model = mongoose.model('FilesSchema', FilesSchema)
module.exports = model;