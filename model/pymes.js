const mongoose = require('mongoose');

const PymeSchema = new mongoose.Schema({
    nombrePyme: {
        type: String
    },
    nombreContacto: {
        type: String
    },
    direccionPyme: {
        type: String
    },
    emailPyme: {
        type: String
    },
    telefonoPyme: {
        type: String
    },
    comentarios: {
        type: String
    }
},

    { collection: 'pyme' }

)

const model = mongoose.model('PymeSchema', PymeSchema);
module.exports = model;