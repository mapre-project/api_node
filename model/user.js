const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: String,
    nombre: String,
    alias: String,
    departamento: String,
    biografia: String,
    rol: String,
    fechaDeRegistro: String,
    imgPerfil: String
},

    { collection: 'user' }

)

const model = mongoose.model('UserSchema', UserSchema);
module.exports = model;