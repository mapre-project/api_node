const mongoose = require('mongoose');

const FAQSchema = new mongoose.Schema({
        title: {
            type: String,
            unique: true
        },
        detail: {
            type: String
        }
    },

    {
        collection: 'faqs',
        timestamps: true
    }

)

const model = mongoose.model('FAQSchema', FAQSchema);
module.exports = model;